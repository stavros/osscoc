#!/usr/bin/env python3
from pathlib import Path

import mistune


TEMPLATE = """
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content= "width=device-width, initial-scale=1">
        <meta charset="utf-8" />
        <link rel="stylesheet" href= "typesettings-1.7-min.css">
        <title>The Open Source Software Code of Conduct</title>

        <style>
            .typesettings.sans-serif h1 {
                font-size: 2.4em;
                margin-bottom: 0.3em;
            }
            .separator {
                margin-bottom: 1em;
            }
            footer {
                padding: 3em 0;
                text-align: center;
                font-size: 1.4em;
                border-top: 1px solid #e1e1e1;
            }
            footer a {
                border-bottom: none;
            }
            a:hover {
                color: #222;
                border-bottom-color: #555;
            }
            @media only screen and (min-width: 1441px) {
                .typesettings {
                    font-size: 2.4em;
                }
            }
            .typesettings {
                max-width: 33em;
                margin: 0 auto;
            }
            a {
                color: #aeaeae;
                border-bottom: 1px dotted #aeaeae;
                text-decoration: none;
            }
        </style>
    </head>
    <body>
        <article class="typesettings sans-serif golden">

%s

        </article>
        <footer>
            <a href="https://gitlab.com/stavros/osscoc">The Code itself is OSS,
            contribute to it</a><br /><br />
            <a rel="license"
              href="http://creativecommons.org/licenses/by-sa/2.0/">
            <img alt="Creative Commons License" style="border-width:0"
              src="https://i.creativecommons.org/l/by-sa/2.0/80x15.png" /></a>
        </footer>
    </body>
</html>
"""


def render_file(infile: Path, outfile: Path) -> None:
    with infile.open() as infile:
        md = infile.read()

    rendered = mistune.markdown(md, escape=False)
    html = TEMPLATE % rendered
    with outfile.open("w") as outfile:
        outfile.write(html.strip())


def main():
    for filename in Path(".").glob("content/*.md"):
        print("Rendering %s..." % filename.name)
        render_file(filename, Path("pages") / (filename.stem + ".html"))

    print("Done.")


if __name__ == "__main__":
    main()
