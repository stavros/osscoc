# The <abbr title="Open Source Software">OSS</abbr> Code of Conduct

## Guidelines to remember

<div class="separator">&nbsp;</div>

Dear Open Source contributor,  
First of all, thank you for your contribution! You are probably here because you are contributing to Open Source
Software, maybe with a comment or a pull request.

Please remember to be civil.
Open Source contributors (like yourself) are volunteers, and the result usually comes with no guarantee.

Also, no swearing!
